#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

int main(int argc, char** argv)
{
    //Initialization
    if (argc != 3)
    {
        std::cerr << "usage: olhaplaca.exe <image_path> <size_multiplier>" << std::endl;
        return -1;
    }

    std::string imagePath(argv[1]);
    double sizeMultiplier = 0;
    
    std::stringstream ss;
    ss << argv[2];
    ss >> sizeMultiplier;
    ss.clear();
    
    cv::Size plateSize(400 * sizeMultiplier, 130 * sizeMultiplier);
    cv::Size alphabeticCharacterSize(54 * sizeMultiplier, 63 * sizeMultiplier);
    double acceptedError = 0.8;

    cv::Mat image = cv::imread(imagePath, cv::IMREAD_GRAYSCALE);
    if (!image.data)
    {
        std::cerr << "No image data!" << std::endl;
        return -1;
    }

    //Pre-processing
    cv::GaussianBlur(image, image, cv::Size(3, 3), 0, 0);
    cv::equalizeHist(image, image);
    cv::Mat mask(image.clone());

    //Processing
    cv::Mat topHatElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(alphabeticCharacterSize.width * 2, 1));
    cv::Mat closeElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(alphabeticCharacterSize.width / acceptedError, 1));
    cv::Mat openingElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(plateSize.width * acceptedError, alphabeticCharacterSize.height * acceptedError));

    cv::morphologyEx(mask, mask, cv::MORPH_TOPHAT, topHatElement);
    cv::threshold(mask, mask, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    cv::morphologyEx(mask, mask, cv::MORPH_CLOSE, topHatElement);
    cv::morphologyEx(mask, mask, cv::MORPH_OPEN, openingElement);
    
    //Segmentation and last check
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(mask, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    std::vector<cv::Point> contours_poly;
    for(unsigned int i = 0; i < contours.size(); ++i)
    {
        approxPolyDP(cv::Mat(contours[i]), contours_poly, 5, true);
        cv::Rect bb = cv::boundingRect(cv::Mat(contours_poly));

        if (bb.width < ((plateSize.width) / acceptedError) && bb.height < (alphabeticCharacterSize.height / acceptedError))
        {
            bb.y -= 1;
            bb.height += 2;
            cv::Mat bbImg(image(bb));            
            std::stringstream ss;
            ss << "plate_" << i << ".jpg";
            cv::imwrite(ss.str(), bbImg);
            std::cout << "Possible plate found: " << ss.str() << std::endl;
        }
    }

    return 0;
}
