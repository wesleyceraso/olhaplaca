# OlhaPlaca [![build status](https://gitlab.com/ci/projects/9366/status.png?ref=master)](https://gitlab.com/ci/projects/9366?ref=master)

OlhaPlaca is a license plate finder. It's been developed targeting Brazilian license plates.

## Quickstart
Clone (https://gitlab.com/wesleyceraso/olhaplaca):

	git clone https://gitlab.com/wesleyceraso/olhaplaca.git

Configure:

	cd olhaplaca
	mkdir build
	cd build
	cmake ../

Build:

	cmake --build

Test:

    ctest

## Contributing

Please contact me through e-mail:

Wesley Ceraso Prudencio (wesleyceraso@gmail.com)